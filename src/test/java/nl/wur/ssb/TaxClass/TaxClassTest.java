package nl.wur.ssb.TaxClass;

import junit.framework.TestCase;

/** Unit test for Assembly. */
public class TaxClassTest extends TestCase {
    /**
     * Testcase 1, paired end reads
     *
     * @throws Exception
     */
    public void testMetaGenomeEndpoint() throws Exception {
        String endpoint = "http://10.117.11.77:7200/repositories/metagenomics_soil";
        String output = "";

        String[] args = {"-endpoint", endpoint, "-output", output, "-debug"};
        AppCAT.main(args);
    }
}