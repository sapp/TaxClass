package nl.wur.ssb.TaxClass;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import life.gbol.domain.AnnotationResult;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.Generic;
import nl.wur.ssb.SappGeneric.ImportProv;
import nl.wur.ssb.SappGeneric.Store;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Parameters(commandDescription = "Available options: ")
public class CommandOptionsTaxClass {

    @Parameter(names = "--help")
    boolean help = false;

    // Creation of global disk store to store RDF data
    public final Domain domain = Store.createDiskStore();
    // Storing annotation provenance object
    public AnnotationResult annotResult;

    final String sappversion = Generic.getVersion(CommandOptionsTaxClass.class)[0];
    final String sappcommit = Generic.getVersion(CommandOptionsTaxClass.class)[1];

    private final String tool = "Taxonomic Class assigner";
    private final String toolversion = sappversion;

    private LocalDateTime starttime = LocalDateTime.now();
    public String sapp = "SAPP TaxClass";

    // @Parameter(names = "-starttime", description = "Start time of code", hidden = true)
    // private LocalDateTime starttime = LocalDateTime.now();

    @Parameter(names = {"-u", "-uniprot"}, description = "SPARQL endpoint of UniProt")
    public String uniprot = "";


    @Parameter(names = {"-e", "-endpoint"}, description = "SPARQL endpoint")
    public String endpoint;


    @Parameter(names = {"-i", "-input"}, description = "Input HDT file")
    public File input;

    // Output
    @Parameter(names = {"-o", "-output"}, description = "Output HDT file", required = true)
    public File output;

    // Cutoff
    @Parameter(names = {"-c", "-cutoff"}, description = "Sum bit score cutoff")
    public double cutoff = 0.5;

    public ImportProv origin;

    private String commandLine;

    private final String repository = "https://gitlab.com/sapp/assembly";
    public final String description = "Assembly module for SAPP";

    @Parameter(names = "-debug", description = "Debug mode")
    public boolean debug = false;

    public ImportProv importProv;

    public CommandOptionsTaxClass(String args[]) throws Exception {
        try {
            new JCommander(this, args);
            commandLine = StringUtils.join(args, " ");


            List<File> files = new ArrayList<>();
            files.add(new File(endpoint));
            origin = new nl.wur.ssb.SappGeneric.ImportProv(domain, files, this.output, this.commandLine, this.tool, this.toolversion, this.repository, null, this.starttime, LocalDateTime.now());

            annotResult = domain.make(AnnotationResult.class, "http://gbol.life/0.1/" + UUID.randomUUID());

            origin.linkEntity(annotResult);
            //

        } catch (ParameterException pe) {
            if (this.help) {
                new JCommander(this).usage();
                System.exit(0);
            }

            System.out.println(pe.getMessage());
            new JCommander(this).usage();
            System.out.println("  * required parameter");
            System.exit(1);
        }
    }
}
