package nl.wur.ssb.TaxClass;

import life.gbol.domain.Contig;
import life.gbol.domain.Organism;
import life.gbol.domain.Rank;
import life.gbol.domain.Taxon;
import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.Generic;
import nl.wur.ssb.SappGeneric.Store;
import org.apache.log4j.Logger;
import org.rdfhdt.hdt.hdt.HDT;
import org.rdfhdt.hdt.hdt.HDTManager;

import java.util.*;
import java.util.function.ToDoubleFunction;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AppCAT {

    private static CommandOptionsTaxClass arguments;
    private static Logger logger;
    private static HDT taxonomy;
    private static final Pattern taxidPattern = Pattern.compile("OX=([0-9]+)");

    // Superkingdom : Bacteria : 450
    private static HashMap<String, HashMap<String, Double>> overallScore = new HashMap<>();

    public static void main(String[] args) throws Exception {

        // Parsing the input arguments
        arguments = new CommandOptionsTaxClass(args);

        // Setting the log level based upon debug argument
        logger = Generic.Logger(arguments.debug);
        logger = Generic.Logger(false);

        // Mounts the taxonomy HDT
        taxonomy = HDTManager.mapIndexedHDT("taxonomy.hdt", null);

        RDFSimpleCon rdfResource;
        // Endpoint or local file check
        Iterator<ResultLine> contigs;
        // Obtain list of contigs from endpoint
        if (arguments.input != null) {
            // HDT
            rdfResource = new RDFSimpleCon("");
            org.rdfhdt.hdt.hdt.HDT hdt = HDTManager.mapIndexedHDT(arguments.input.getAbsolutePath(), null);
            contigs = rdfResource.runQuery(hdt, "getDNAObjects.txt", false, "some par").iterator();
        } else if (arguments.endpoint != null) {
            logger.info("Connecting to sparql endpoint");
            // Endpoint
            // Something goes wrong with http://
            String endpoint = arguments.endpoint;
            rdfResource = new RDFSimpleCon(endpoint);
            contigs = rdfResource.runQuery("getDNAObjects.txt", false).iterator();
        } else {
            throw new Exception("Either use -input or -endpoint");
        }

        // Check if dir exists (just make it)
        // Files.createDirectories(Paths.get("./UniProtProteins/"));

        // Retrieval of protein files per contig
        while (contigs.hasNext()) {
            // What needs to be reset:

            // Store for the scores
            overallScore = new HashMap<>();
            // The average bit score on contig level
            double totalAverage = 0.0;

            ResultLine contig = contigs.next();
            // sappgeneric specify which type

            Contig c = arguments.domain.make(life.gbol.domain.Contig.class, contig.getIRI("contig"));
            Organism organism = arguments.domain.make(life.gbol.domain.Organism.class, contig.getIRI("contig") + "/TaxClass/organism");
            Taxon taxon = arguments.domain.make(life.gbol.domain.Taxon.class, "http://gbol.life/taxon/species/Solibacillus_silvestris");
            taxon.setTaxonRank(Rank.RankSpecies);
            taxon.setTaxonName("Solibacillus_silvestris");

            organism.addTaxonomyLineage(taxon);
            c.setOrganism(organism);
            System.out.println(contig.getIRI("contig"));

            // This should be > 90% of the maximum bit score for each protein...

            // Thus first get all proteins
            Iterator<ResultLine> proteins = rdfResource.runQuery("getProteins.txt", false, contig.getIRI("contig")).iterator();

            // For each protein calculate the LCA
            while (proteins.hasNext()) {
                ResultLine protein = proteins.next();
                String proteinIRI = protein.getIRI("protein");

                // Gives the protein hits with 90% cutoff
//                System.out.println(proteinIRI);
                Iterator<ResultLine> homologoues = rdfResource.runQuery("getProteinHit.txt", false, proteinIRI).iterator();


                /**
                 * For each protein get all bit scores and take the average of that...
                 * Secondly get the LCA lineage
                 */

                // TODO check what when you hit a protein twice for some reason?
                List<Double> bitscores = new ArrayList<>();
                Set<String> accessions = new HashSet<>();

                // For all the hits, collect the TAXID + BITSCORE
                while (homologoues.hasNext()) {
                    ResultLine homologue = homologoues.next();
                    String description = homologue.getLitString("to");
                    String taxid = null;
                    if (!description.contains("OX=")) {
                        // ignore me
                    } else {
                        Matcher matcher = taxidPattern.matcher(description);
                        while (matcher.find()) {
                            accessions.add(matcher.group(1));
                            bitscores.add(homologue.getLitDouble("bitscore"));
                        }
                    }
                }
//                System.out.println(bitscores);
                // Now obtain the overall taxonomic representation?...
                HashMap<String, ArrayList<String>> lineages = LCA(accessions);

                // Take the average bitscore
                double average = bitscores.stream().mapToDouble(val -> val).average().orElse(0.0);

                // Of total average * 0.5 in the end... and only when we have a commonality at least at superkingdom level
                if (lineages.get("Superkingdom").size() > 0) {
                    totalAverage = totalAverage + average;
                }
                // Add average bitscore for each element in each lineage (e.g. Superkingdom)
                for (String lineage : lineages.keySet()) {
                    // e.g. Bacteria
                    for (String element : lineages.get(lineage)) {
                        // If lineage not in the overall score (e.g. SuperKingdom) we initialize it with a hashmap
                        // Genus : <Name1 : Value> , <Name2 : Value>
                        if (overallScore.get(lineage) == null) {
                            overallScore.put(lineage, new HashMap<>());
                        }
                        // if name of the lineage not in the overall score (e.g. Streptococcus) we initalize it with 0.0
                        if (overallScore.get(lineage).get(element) == null) {
                            overallScore.get(lineage).put(element, 0.0);
                        }
                        // We add the value to the Lineage (Genus) and Element (Streptococcus)
                        double value = overallScore.get(lineage).get(element) + average;
                        overallScore.get(lineage).put(element, value);
                    }
                }
            }

            // FINAL SCORE
            totalAverage = totalAverage * arguments.cutoff; // Should be 0.5?

            // Lineage
            ArrayList<String> lineageArray = new ArrayList<>();
            lineageArray.add("Superkingdom");
            lineageArray.add("Phylum");
            lineageArray.add("Class");
            lineageArray.add("Order");
            lineageArray.add("Family");
            lineageArray.add("Genus");
            lineageArray.add("Species");

            //            Domain domain = Store.createDiskStore();
            //            Scaffold scaffold = domain.make(life.gbol.domain.Scaffold.class,"contigIRI");
            //            Organism organism = domain.make(life.gbol.domain.Scaffold.class,"contigIRI/organism");
            //            organism.setScientificName();
            //            organism.addTaxonomyLineage();
            //            scaffold.setOrganism()

            // Order of the lineage...
            List<String> lineages = Arrays.asList(new String[]{"Superkingdom", "Phylum", "Class", "Order", "Family", "Genus", "Species"});
//            System.out.println("#### " + contig.getIRI("contig"));
            for (String lineage : lineages) {
                // Should be one consensus right?
                // System.out.println(overallScore.get(lineage));
                // Sometimes a lineage does not have genus or something else...
                if (overallScore.get(lineage) != null) {
                    // Ignores lineages with more than one member LCU
//                    if (overallScore.get(lineage).size() > 1)
                    //   continue;

                    for (String element : overallScore.get(lineage).keySet()) {
                        if (overallScore.get(lineage).get(element) > totalAverage) {
//                            logger.info(lineage + " # " + element + " # " + overallScore.get(lineage).get(element)+ " # " + totalAverage + " # " + lineageArray);
                            // If this gives an array index exception it means we had two elements that bypass the cutoff
                            // Therefor this index is erased and we break the for loop
                            try {
//                                logger.info(1 + " " + lineageArray);
//                                logger.info(lineage);
//                                logger.info(element);
                                lineageArray.set(lineageArray.indexOf(lineage), element);
                                //System.out.println(LineageArray.indexof(lineage));
//                                logger.info(2 +" " + lineageArray);
                            } catch (ArrayIndexOutOfBoundsException e) {
                                // Reset back to the lineage
//                                logger.info(3 +" " + lineageArray);
                                lineageArray.set(lineages.indexOf(lineage), lineage);
                                break;
                            }
                        }
                    }
                }
            }
            logger.info(org.apache.commons.lang3.StringUtils.join(lineageArray, "; "));
//            if (lineageArray.contains("Methanobrevibacter")) {
//                System.out.println(overallScore);
//                System.exit(0);
//            }
        }
    }


    /**
     * Retrieves a list of taxid accession
     *
     * @param accessions list of taxon ids
     * @return lineage hashmap
     * @throws Exception
     */
    private static HashMap<String, ArrayList<String>> LCA(Set<String> accessions) throws Exception {
        // Loading the hits from one protein into local endpoint
        Domain uniprotStore = Store.createMemoryStore();

        // Integer can be removed, this can become a HashMap<String, List>
        HashMap<String, HashMap<String, Integer>> store = new HashMap<>();
        store.put("Superkingdom", new HashMap<>());
        store.put("Phylum", new HashMap<>());
        store.put("Class", new HashMap<>());
        store.put("Order", new HashMap<>());
        store.put("Family", new HashMap<>());
        store.put("Genus", new HashMap<>());
        store.put("Species", new HashMap<>());

        // Use HDT query on Taxonomy here...
        for (String accession : accessions) {
            try {
                String taxonIRI = "http://purl.uniprot.org/taxonomy/" + accession;

                Iterable<ResultLine> lineages = uniprotStore.getRDFSimpleCon().runQuery(taxonomy, "getLineage.txt", taxonIRI);

                if (lineages != null) {
                    Iterator<ResultLine> lineagesIter = lineages.iterator();
                    while (lineagesIter.hasNext()) {
                        ResultLine lineage = lineagesIter.next();
                        String rank = lineage.getIRI("rank").replaceAll(".*/", "");
//                        if (rank.matches("Superkingdom"))
//                            AppCAT.logger.info(taxonIRI + " # " + rank + " # " + lineage.getLitString("className"));
                        if (store.get(rank) == null) {
                            HashMap<String, Integer> rankValue = new HashMap<>();
                            store.put(rank, rankValue);
                        }
                        if (store.get(rank).get(lineage.getLitString("className")) == null) {
                            store.get(rank).put(lineage.getLitString("className"), 0);
                        }
//                        System.out.println(rank + "\t" + lineage.getLitString("className"));
                    }
                }
            } catch (NullPointerException e) {
            }
        }

        // When having a hit with Bacteria & Viruses or any other combination we cannot really do much...?
        String level;
        HashMap<String, ArrayList<String>> lineage = new HashMap<>();
        // Init
        lineage.put("Superkingdom", new ArrayList<>());
        lineage.put("Phylum", new ArrayList<>());
        lineage.put("Class", new ArrayList<>());
        lineage.put("Order", new ArrayList<>());
        lineage.put("Family", new ArrayList<>());
        lineage.put("Genus", new ArrayList<>());
        lineage.put("Species", new ArrayList<>());

        // Adds the name to the lineage but only if one hit was found...
        if (store.get("Superkingdom").size() == 1) {
            level = "Superkingdom";
            lineage.get(level).addAll(store.get(level).keySet());
        } else if (store.get("Superkingdom").size() != 1) {
            return lineage;
        }

        if (store.get("Phylum").size() == 1) {
            level = "Phylum";
            lineage.get(level).addAll(store.get(level).keySet());
        } else if (store.get("Phylum").size() != 1) {
            return lineage;
        }

        if (store.get("Class").size() == 1) {
            level = "Class";
            lineage.get(level).addAll(store.get(level).keySet());
        } else if (store.get("Class").size() != 1) {
            return lineage;
        }

        if (store.get("Order").size() == 1) {
            level = "Order";
            lineage.get(level).addAll(store.get(level).keySet());
        } else if (store.get("Order").size() != 1) {
            return lineage;
        }

        if (store.get("Family").size() == 1) {
            level = "Family";
            lineage.get(level).addAll(store.get(level).keySet());
        } else if (store.get("Family").size() != 1) {
            return lineage;
        }

        if (store.get("Genus").size() == 1) {
            level = "Genus";
            lineage.get(level).addAll(store.get(level).keySet());
        } else if (store.get("Genus").size() != 1) {
            return lineage;
        }

        if (store.get("Species").size() == 1) {
            level = "Species";
            lineage.get(level).addAll(store.get(level).keySet());
        } else if (store.get("Species").size() != 1) {
            return lineage;
        }

        // logger.info("LCA: " + lineage);
        return lineage;
    }
}
