PREFIX gbol: <http://gbol.life/0.1/>
SELECT ?protein
WHERE {
    VALUES ?contig { <%1$s> }
	?sample a gbol:Sample .
	?contig gbol:sample ?sample .
    ?contig gbol:feature ?gene .
    ?gene gbol:transcript ?transcript .
    ?transcript gbol:feature ?cds .
    ?cds gbol:protein ?protein .
}

